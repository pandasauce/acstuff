# Assetto Corsa Tools

## `acsm_car_packer.py`
Prepares car packs for the "Install missing content" button feature in Content Manager.

Will pattern-match cars in the target directory and package them into standalone 7-Zip archives,
using car ID as the archive name.

If you supply `--base_url`, it will patch `ui_car.json` prior to packing. 
You can then upload these cars to your server and it will automatically use these download URLs.

See `-h` for all configurable options.

## Copyright
AGPL-3.0

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[GNU Affero General Public License](./LICENSE) for more details.
