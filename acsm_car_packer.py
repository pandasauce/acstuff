import argparse
import json
import os
import subprocess
import sys
from pathlib import Path
from typing import Optional


def patch_base_url(json_path: Path, car_name: str, base_url: str):
    print(f"Patching {car_name} with a download URL")
    fh_r = open(json_path, "r", encoding="utf-8")
    ui_json = json.load(fh_r)
    ui_json["downloadURL"] = f"{base_url.rstrip('/')}/{car_name}.7z"
    fh_r.close()
    fh_w = open(json_path, "w", encoding="utf-8")
    json.dump(ui_json, fh_w, indent=2)
    fh_w.close()


def pack_cars(start_dir: Path, zipper: str, destination: Path, include_drivers: bool,
              base_url: Optional[str] = None,
              prefix: Optional[str] = None):
    # some car packs are annoying as fuck and ship with custom driver models, stored separately from the car
    # this means we have to implement heuristics or those cars will crash if downloaded from our server

    if include_drivers:
        # -> start dir is ACROOT
        cars_dir = start_dir.joinpath("content").joinpath("cars")
        archive_car_prefix = Path("content").joinpath("cars")
        zipper_cwd = start_dir  # zip root is ACROOT
    else:
        if start_dir.joinpath("content").exists():
            # -> we are in ACROOT and don't need drivers -> step into CONTENT
            start_dir = start_dir.joinpath("content")
        if start_dir.joinpath("cars").exists():
            # -> start dir is CONTENT
            cars_dir = start_dir.joinpath("cars")
            archive_car_prefix = Path("cars")
            zipper_cwd = start_dir  # zip root is CONTENT
        else:
            # -> start dir is CARS
            cars_dir = start_dir
            archive_car_prefix = Path("")
            zipper_cwd = start_dir  # zip root is CARS

    with os.scandir(cars_dir) as d:
        for car_dir in d:
            if not car_dir.is_dir():
                continue
            car_name = car_dir.name
            if prefix and not car_name.startswith(prefix):
                continue
            json_path = Path(car_dir.path).joinpath("ui").joinpath("ui_car.json")
            if not json_path.exists():
                continue
            if base_url:
                patch_base_url(json_path, car_name, base_url)
            archive_path = destination.joinpath(f"{car_name}.7z")
            print(f"Packing {car_name} to f{archive_path}")
            archive_car_path = archive_car_prefix.joinpath(car_name)
            subprocess.check_call([zipper, "a", archive_path, archive_car_path], cwd=zipper_cwd)
            if include_drivers:
                subprocess.check_call([zipper, "a", archive_path, "content/driver/"], cwd=zipper_cwd)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("start_dir", type=str,
                        help="Starting directory to search for cars. "
                             "Supported: AC root directory, content, content/cars or equivalent")
    parser.add_argument("--include-drivers", default=False, action="store_true",
                        help="Include the driver models. "
                             "If enabled, starting directory MUST be AC root or equivalent")
    parser.add_argument("--base-url", type=str, required=False,
                        help="If provided, ui_car.json will be patched to enable ACSM downloads")
    parser.add_argument("--prefix", type=str, required=False,
                        help="Car ID must start with this string, e.g. excite_")
    parser.add_argument("--zipper", type=str, required=False, default="7z",
                        help="Path to 7zip binary. Defaults to '7z'")
    parser.add_argument("--destination", type=str, required=False, default=os.getcwd(),
                        help="Where the .7z files will be placed. Defaults to current directory")
    args = parser.parse_args()

    resolved_path = Path(args.start_dir).absolute()
    if args.include_drivers and not resolved_path.joinpath("content").exists():
        print("ERROR: To include drivers, starting directory MUST be AC root or equivalent", file=sys.stderr)
        return
    resolved_destination = Path(args.destination).absolute()
    try:
        pack_cars(resolved_path, args.zipper, resolved_destination, args.include_drivers, args.base_url, args.prefix)
    except subprocess.CalledProcessError as err:
        print(f"ERROR: could not pack the cars. {str(err)}", file=sys.stderr)


if __name__ == "__main__":
    main()
